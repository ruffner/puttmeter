#include <ADXL362.h>
#include <SPI.h>
#include <Adafruit_NeoPixel.h>
#include <Wire.h>
#include "nRF24L01.h"
#include <RF24.h>

#define PAYLOAD_SIZE 16
#define NUM_LEDS 20
#define LED_PIN 19

#define WAKEUP 0xAA
#define SLEEP  0xBB
#define ACK    0xCC
#define GOAL   0xDD

#define MODE_MEASURE 0
#define MODE_GAME    1

// accelerometer
ADXL362 xl; 
 // radio
RF24 radio(14,15);
// light strip
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);

// recieved data
uint8_t rec_payload[PAYLOAD_SIZE];
// address of putter
uint32_t putter_addr = 0xaabbccLL;
// address of putting green
uint32_t green_addr = 0xaabbaaLL;

int mode = MODE_MEASURE;
uint8_t sig_wakeup[16] = {0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA, WAKEUP};
uint8_t sig_sleep[16] = {0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA, SLEEP};
uint8_t sig_goal[16] = {0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA, GOAL};
uint8_t sig_ack[16] = {0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA, ACK};

int goal_count = 0;

int16_t temp;
int16_t XValue, YValue, ZValue, Temperature;

void setup(){  
  // led strip setup
  strip.begin();
  strip.show();
  
  Serial.println("Starting...");
  
  jiggle(1000);
  
  // nrf24l01+ setup
  radio.begin();
  radio.setAutoAck(false);
  radio.setDataRate(RF24_1MBPS);
  radio.setPayloadSize(PAYLOAD_SIZE);
  radio.setChannel(1);
  radio.setPALevel(RF24_PA_HIGH);
  radio.setCRCLength(  RF24_CRC_16 );
  radio.openWritingPipe( green_addr );
  radio.openReadingPipe(1, putter_addr );
  radio.startListening();
  
  // accelerometer setup
  xl.begin(10);    // Setup SPI protocol, issue device soft reset
  xl.setRange(ADXL362_2G);
  xl.setODR(ADXL362_RATE_200);
  xl.beginMeasure();              // Switch ADXL362 to measure mode  
}

void loop() {
  // read all three axis in burst to ensure all measurements correspond to same sample time
  xl.readXYZTData(XValue, YValue, ZValue, Temperature);  
  /*Serial.print(XValue);	 
  Serial.print(" ");
  Serial.print(YValue);	 
  Serial.print(" ");
  Serial.println(ZValue);*/
  
  if(XValue < -1000) {
    unsigned long time = millis();
    boolean swap = true;
    while(millis() - time < 1000) {
      xl.readXYZTData(XValue, YValue, ZValue, Temperature);
      if(XValue > -950) {
        swap = false;
        break;
      }
    }
    if(swap) {
      mode = !mode;
      
      if(mode == MODE_MEASURE) {
        send_sleep();
        ramp(strip.Color(255, 0, 0));
      } else {
        send_wakeup();
        ramp(strip.Color(0,0,255));
        goal_count = 0;
      }
      
      Serial.print("mode: ");
      Serial.println(mode);
    }
  }
  
  if(mode == MODE_MEASURE) {
    long m = XValue - 1050;
    m = max(0,m);
    m = m/20;
    
    setScore(m, strip.Color(10, 200, 0));
   // correlate leds to x axis strength
   
   
  } else {
    if(radio.available()) {
      radio.read( rec_payload, PAYLOAD_SIZE );
       
      if(rec_payload[15] == GOAL) {
        radio.stopListening();
        radio.write(&sig_ack, PAYLOAD_SIZE);
        radio.startListening(); 
        
        Serial.println("recieved GOAL!");
       
        goal_count++;
        Serial.print("  goal count: ");
        Serial.println(goal_count);
        jiggle(1000);
        setScore(goal_count, strip.Color(51, 204, 255));
        //prevent accidental doubles
        delay(500);
        
      }
   
   
    } 
  
 
  	  
  }
}

void send_sleep() {
  radio.stopListening();
  radio.write( &sig_sleep, PAYLOAD_SIZE);  
  radio.startListening();
 
  unsigned long time = millis();
  boolean ack = false;
  while(millis() - time < 250) {
    if(radio.available()) {
      // Fetch the payload, and see if this was the last one.
      radio.read( rec_payload, PAYLOAD_SIZE );
      ack = true;
      break;
    }
  }
 
  if(ack) {
    Serial.println("got ack from sleep");
  } else {
    Serial.println("no ack from sleep");
  } 
}

void send_wakeup() {  
  radio.stopListening();
  radio.write( &sig_wakeup, PAYLOAD_SIZE);   
  radio.startListening();
 
  unsigned long time = millis();
  boolean ack = false;
  while(millis() - time < 250) {
    if(radio.available()) {
      // Fetch the payload, and see if this was the last one.
      radio.read( rec_payload, PAYLOAD_SIZE );
      ack = (rec_payload[15] == ACK);
      break;
    }
  }
 
  if(ack) {
    Serial.println("got ack from wakeup");
  } else {
    Serial.println("no ack from wakeup");
  }
}

void setScore(int score, uint32_t color) {  
  for( int i = 0; i < strip.numPixels(); i++) {
    if(i < score)
      strip.setPixelColor(i, color);
    else
      strip.setPixelColor(i, strip.Color(0, 0, 0));
    strip.show();
  }   
}

void jiggle(unsigned long duration) {
  unsigned long st = millis();
  int tog = 0;
  uint32_t c1 = strip.Color(0, 136, 220);
  uint32_t c2 = strip.Color(220, 130, 0);
  while(millis() - st < duration) {
    for( int i = 0; i < strip.numPixels(); i++ ) {
      strip.setPixelColor(i, tog ? c1 : c2);
      strip.show();
      tog = !tog;
    }
    tog = !tog;
    delay(100);
  }
  
  for( int i = 0; i < strip.numPixels(); i++ )
    strip.setPixelColor(i, strip.Color(0,0,0));
  
  strip.show();
}

void ramp(uint32_t color) {
  uint32_t black = strip.Color(0,0,0);
  for( int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, color);
    strip.show();
    delay(15);
  }
  for( int i = NUM_LEDS-1; i >= 0; i--) {
    strip.setPixelColor(i, black);
    strip.show();
    delay(15);
  }
}
