%Open Serial COM Port
s = serial('COM7');
set(s,'BaudRate',9600,'Databits',8,'Parity','none','StopBits', 1,'FlowControl', 'none','Terminato','CR');
fopen(s);
s.ReadAsyncMode='continuous';

cycles = 1000;
global x;
axes = 0:cycles:1;

n = 0;
i = 1;
while(n < cycles)
    coords = fscanf(s);
    t = str2num(coords);
    
    
    temp = [t(1) t(2) t(3)];
    
    % with z data
    %temp = [t(1) t(2) t(3)]; 
    x(i,:) = temp;
    %x(:,) = diff(x(:,1));
   
    i = i+1;
    
    set(gcf, 'color', 'white');
    plot(x), axis([1 cycles -2048 2048])
    drawnow;
    grid on;                        %Grid lines for 2-D and 3-D plots.
    title('Accelerometer Data');               %Add title to current axes.
    xlabel('Time');                 %Label x-axis.
    ylabel('XYZ Values');         %Label y-axis.
    n = n + 1;
    
end





%Close Serial COM Port and Delete useless Variables
fclose(s);

disp('Session Terminated...');