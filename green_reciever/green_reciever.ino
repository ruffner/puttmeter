#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

#define LASER_PIN 8

#define PAYLOAD_SIZE 16

#define WAKEUP 0xAA
#define SLEEP  0xBB
#define ACK    0xCC
#define GOAL   0xDD

RF24 radio(9,10);

uint8_t rec_payload[PAYLOAD_SIZE];

uint32_t putter_addr = 0xaabbccLL;
uint32_t green_addr  = 0xaabbaaLL;

bool active = false;

unsigned long tfg;

void setup(void)
{
  // power the light sensor
  pinMode(A4, OUTPUT);
  digitalWrite(A4, HIGH);
  
  // power on the laser
  pinMode(LASER_PIN, OUTPUT);  
  
  Serial.begin(9600);
  
  Serial.println("Starting...");

  radio.begin();
  radio.setAutoAck(false);
  radio.setDataRate(RF24_1MBPS);
  radio.setPayloadSize(PAYLOAD_SIZE);
  radio.setChannel(1);
  radio.setCRCLength(  RF24_CRC_16 );
  radio.setPALevel(RF24_PA_HIGH);
  radio.openReadingPipe(1, green_addr);
  radio.openWritingPipe( putter_addr );
  radio.startListening();
  
  Serial.print("A3 value: ");
  Serial.println(analogRead(A3));
  
  Serial.println("listening...");
}

void loop(void)
{
  if(radio.available()) {
    Serial.println("got something");
    
      // Fetch the payload, and see if this was the last one.
      radio.read( rec_payload, PAYLOAD_SIZE );

      if(rec_payload[15] == WAKEUP) {
        radio.stopListening();
        
        active = true;
        digitalWrite(LASER_PIN, HIGH);
        
        Serial.println("waking up...");

        uint8_t resp[16];
        resp[15] = ACK;
        radio.write( &resp, PAYLOAD_SIZE );
        
        radio.startListening();
      } else if(rec_payload[15] == SLEEP) {
        radio.stopListening();
        
        active =  false;
        digitalWrite(LASER_PIN, LOW);
        
        Serial.println("sleeping...");
                
        uint8_t resp[16];
        resp[15] = ACK;
        radio.write( &resp, PAYLOAD_SIZE);
     
        radio.startListening(); 
      }

  }
  
  if(active && millis() - tfg > 1000) {
    //Serial.print("A3 value: ");
    //Serial.println(analogRead(A3));
  
    unsigned long time = millis();
    
    while(analogRead(A3) > 900) {
     if(millis() - time > 300) {
       Serial.println("GOAL DETECTED - SENDING");
       
       radio.stopListening();
       
       uint8_t resp[16];
       resp[15] = GOAL;
       
       radio.write( &resp, PAYLOAD_SIZE);
       radio.startListening();
       tfg = millis();
       break;
     }
    }
  } else if(!active) {
    digitalWrite(LASER_PIN, LOW);
  }
}
